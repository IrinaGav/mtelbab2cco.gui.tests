﻿namespace mtelbab2cco.gui.tests.Data
{
    class ErrorMessages
    {
        internal static string IncorrectEmail = "Unesite validnu e-mail adresu.";
    }

    class Notifications
    {
        internal static string NotUniqueEmail = "Već ste prijavljeni na newsletter.";
    }
}
