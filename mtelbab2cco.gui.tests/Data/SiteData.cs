﻿using System.Collections.Generic;
using System.Configuration;
using NUnit.Framework;

namespace mtelbab2cco.gui.tests.Data
{
    class SiteData
    {
        internal static string BaseURL = ConfigurationManager.AppSettings.Get("baseUrl");

        internal static string PlpURL = BaseURL + "Privatni/Uredjaji/telefoni";
        internal static string PdpURL = BaseURL + "Privatni/Uredjaji/Pojedinacni-uredjaji/telefoni";

        internal static string[] SocialNets = { "facebook", "youtube", "instagram", "linkedin" };
        internal static string[] PromoStickers =
            {
            "NOVO", "AKCIJA", "DOSTUPNO NA UPIT", "SAMO ONLINE", "NEDOSTUPNO NA WEBU", "USKORO", "OUTLET PRODAJA", "NAJPRODAVANIJI", "SNIŽENJE", "POKLON"
            };
    }

    class SiteGuiData
    {
        internal static string ActivePillColor = "rgba(237, 26, 59, 1)";
        internal static string ProductCardCompareButton = "+ Uporedite ";
        internal static string ProductCardCompareButtonColor = "rgba(133, 137, 140, 1)";
        internal static string ProductCardCompareButtonColorIsAdded = "rgba(237, 26, 59, 1)";
    }

    class TestData
    {
        internal static string[] InvalidEmail = new string[]
        {
            "test@test","test@testtest@test","test@t est","test@test.for.","t..e-st@test.com","test.test.com","test@test.1com"
        };

        internal static string[] ValidEmail = new string[] { "test@test.com" };
    }

    class FeatureTestCaseData
    {
        internal static IEnumerable<TestCaseData> InvalidEmails()
        {
            return DataHelper.getFeatureTestCaseData1(TestData.InvalidEmail);
        }

        internal static IEnumerable<TestCaseData> ValidEmails()
        {
            return DataHelper.getFeatureTestCaseData1(TestData.ValidEmail);
        }

        internal static IEnumerable<TestCaseData> SocialNets()
        {
            return DataHelper.getFeatureTestCaseData1(SiteData.SocialNets);
        }
    }

    class DataHelper
    {
        internal static IEnumerable<TestCaseData> getFeatureTestCaseData1(string[] testData)
        {
            foreach (var item in testData)
            {
                yield return new TestCaseData(item);
            }
        }

        internal static IEnumerable<TestCaseData> getFeatureTestCaseData2(string[,] testData)
        {
            for (int i = 0; i < testData.GetLength(0); i++)
            {
                yield return new TestCaseData(testData[i, 0], testData[i, 1]);
            }
        }
    }
}
