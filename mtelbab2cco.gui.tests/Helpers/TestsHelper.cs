﻿using System;
using System.Collections.Generic;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace mtelbab2cco.gui.tests.Helpers
{
    class TestSettings
    {
        internal static int DriverImplicitWaitSec = 1;
        internal static int SleepMillisec = 500;
        internal static int TimesSleep = 10;
    }

    class TestsHelper
    {
        private IWebDriver driver;
        private WebDriverWait wait;

        internal TestsHelper(IWebDriver driver)
        {
            this.driver = driver;
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(TestSettings.DriverImplicitWaitSec));
        }

        internal void StartDriver()
        {
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(TestSettings.DriverImplicitWaitSec);
            driver.Manage().Cookies.DeleteAllCookies();
        }

        internal bool OpenPage(string pageUrl)
        {
            driver.Url = pageUrl;
            return isPageOpened(pageUrl);
        }

        internal bool isPageOpened(string pageUrl)
        {
            return WaitCondition(() => driver.Url == pageUrl);
        }

        internal bool isPageLoaded(IWebElement we)
        {
            return WaitCondition(() => we.GetCssValue("display").Equals("block"));
        }

        internal bool ClickWeb(IWebElement we)
        {
            if (isWebEnabled(we))
            {
                we.Click();
                Thread.Sleep(TestSettings.SleepMillisec);
                return true;
            }
            return false;
        }

        internal bool ClearWeb(IWebElement we)
        {
            if (isWebEnabled(we))
            {
                int nn = we.GetAttribute("value").Length;
                for (int i = 0; i < nn; i++)
                {
                    we.SendKeys(Keys.Backspace);
                }
                return true;
            }
            return false;
        }

        internal bool TypeWeb(IWebElement we, string inputValue)
        {
            if (isWebEnabled(we))
            {
                we.SendKeys(inputValue);
                return true;
            }
            return false;
        }

        internal bool isWebEnabled(IWebElement we)
        {
            return WaitCondition(() => we.Enabled);
        }


        internal bool isWebNotEnabled(IWebElement we)
        {
            return WaitCondition(() => !we.Enabled);
        }

        internal bool isWebDisplayed(IWebElement we)
        {
            return WaitCondition(() => we.Displayed);
        }

        internal string WaitWebText(IWebElement we)
        {
            if (WaitCondition(() => we.Text.Length > 0))
            {
                return we.Text;
            }
            else
            {
                return string.Empty;
            }
        }

        internal bool WaitWebClosed(IWebElement we)
        {
            return WaitCondition(() => we.Text.Length == 0);
        }

        internal bool isWebExists(IList<IWebElement> we)
        {
            return WaitCondition(() => we.Count > 0);
        }

        internal bool isWebNotExists(IList<IWebElement> we)
        {
            return WaitCondition(() => we.Count == 0);
        }

        internal bool WaitWe(Func<object, bool> condition)
        {
            try
            {
                wait.Until(foo => condition);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        internal bool WaitCondition(Func<bool> condition)
        {
            for (int i = 0; i < TestSettings.TimesSleep; i++)
            {
                try
                {
                    if (condition())
                    {
                        return true;
                    }
                    Thread.Sleep(TestSettings.SleepMillisec);
                }
                catch (Exception)
                {
                    continue;
                }
            }
            return false;
        }

        internal int GetRandomIndex(int minValue, int maxValue)
        {
            Random rnd = new Random();
            int value = rnd.Next(minValue, maxValue);
            return value;
        }

        internal bool isValueInArray(string value, string[] arrayToCheck)
        {
            foreach (var item in arrayToCheck)
            {
                if (item.Equals(value))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
