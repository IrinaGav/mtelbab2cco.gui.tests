﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace mtelbab2cco.gui.tests.Pages
{
    class Footer : Page
    {
        public Footer(IWebDriver driver) : base(driver)
        { }

        private IWebElement FooterEmail => driver.FindElement(By.XPath("//*[@class='main-footer']//input[@name='email']"));
        private IWebElement FooterEmailSubmitBtn => driver.FindElement(By.XPath("//*/button[@class='newsletter-submit-btn submit-btn']"));
        private IList<IWebElement> SocialNets => driver.FindElements(By.XPath("//*[@class='social-icons']//li/a"));

        internal bool TypeEmail(string inputValue)
        {
            return TypeWeb(FooterEmail, inputValue);
        }

        internal bool isEmailSubmitBtnEnabled()
        {
            return isWebEnabled(FooterEmailSubmitBtn);
        }

        internal bool ClickEmailSubmitBtn()
        {
            return ClickWeb(FooterEmailSubmitBtn);
        }

        internal int GetSocialNetsCount()
        {
            return SocialNets.Count;
        }

        internal bool isSocialNetExist(int index, string name)
        {
            return SocialNets[index].GetAttribute("href").Contains(name);
        }

        internal bool ClickSocialNet(string name)
        {
            foreach (var item in SocialNets)
            {
                if (item.GetAttribute("href").Contains(name))
                {
                    ClickWeb(item);
                    return true;
                }
            }
            return false;
        }
    }
}
