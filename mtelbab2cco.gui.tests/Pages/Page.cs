﻿using System.Collections.Generic;
using mtelbab2cco.gui.tests.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumExtras.PageObjects;

namespace mtelbab2cco.gui.tests.Pages
{
    class Page
    {
        protected IWebDriver driver;
        protected TestsHelper testsHelper;
        private IWebElement Loader => driver.FindElement(By.XPath("//*[@class='b2c-loader']"));
        private IWebElement Error => driver.FindElement(By.XPath("//*[@id='-error']"));
        private IWebElement Notification => driver.FindElement(By.XPath("//*[@class='vm-flash-message']"));

        internal Page(IWebDriver driver)
        {
            this.driver = driver;
            testsHelper = new TestsHelper(driver);
            PageFactory.InitElements(driver, this);
        }

        internal bool isPageLoaded()
        {
            return testsHelper.isPageLoaded(Loader);
        }

        internal bool ClickWeb(IWebElement we)
        {
            Actions action = new Actions(driver);
            action.MoveToElement(we);
            action.Perform();
            return testsHelper.ClickWeb(we);
        }

        internal bool TypeWeb(IWebElement we, string inputValue)
        {
            testsHelper.ClearWeb(we);
            return testsHelper.TypeWeb(we, inputValue);
        }

        internal string GetErrorMessage()
        {
            return testsHelper.WaitWebText(Error);
        }

        internal string GetNotification()
        {
           return testsHelper.WaitWebText(Notification);
        }

        internal bool WaitNotificationClosed()
        {
           return testsHelper.WaitWebClosed(Notification);
        }

        internal bool isWebEnabled(IWebElement we)
        {
            return testsHelper.isWebEnabled(we);
        }

        internal bool isWebNotEnabled(IWebElement we)
        {
            return testsHelper.isWebNotEnabled(we);
        }

        internal bool isWebDisplayed(IWebElement we)
        {
            return testsHelper.isWebDisplayed(we);
        }

        internal bool isWebExists(IList<IWebElement> we)
        {
            return testsHelper.isWebExists(we);
        }

        internal bool isWebNotExists(IList<IWebElement> we)
        {
            return testsHelper.isWebNotExists(we);
        }
    }
}
