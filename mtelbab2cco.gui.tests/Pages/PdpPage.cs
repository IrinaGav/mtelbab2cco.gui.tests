﻿using OpenQA.Selenium;

namespace mtelbab2cco.gui.tests.Pages
{
    class PdpPage : Page
    {
        public PdpPage(IWebDriver driver) : base(driver)
        { }

        private IWebElement PageContainer => driver.FindElement(By.XPath("//*[@class='device-list-container device-list-content']"));
    }
}
