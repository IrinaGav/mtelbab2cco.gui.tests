﻿using System.Collections.Generic;
using mtelbab2cco.gui.tests.Data;
using OpenQA.Selenium;

namespace mtelbab2cco.gui.tests.Pages
{
    class PlpPage : Page
    {
        public PlpPage(IWebDriver driver) : base(driver)
        { }

        private IWebElement PageContainer => driver.FindElement(By.XPath("//*[@class='device-list-container device-list-content']"));
        private IList<IWebElement> Pills => driver.FindElements(By.XPath("//*[@class='pills-static']"));
        private IList<IWebElement> PillsInactive => driver.FindElements(By.XPath("//*[@class='swiper-slide']"));
        private IList<IWebElement> PillsActive => driver.FindElements(By.XPath("//*[@class='swiper-slide active-slide']"));

        private IList<IWebElement> ProductCards => driver.FindElements(By.XPath("//*[@class='device-item']"));
        private IList<IWebElement> ProductCardBrand => driver.FindElements(By.XPath("//*[@class='brand-name']"));
        private IList<IWebElement> ProductCardName => driver.FindElements(By.XPath("//*[@class='device-name js-device-name']"));
        private IList<IWebElement> ProductAddToCompareButton => driver.FindElements(By.XPath("//*[@class='device-item-add-compare']"));
        private IList<IWebElement> ProductIsAddedToCompareButton => driver.FindElements(By.XPath("//*[@class='device-item-add-compare active']"));
        private IList<IWebElement> ProductStickers => driver.FindElements(By.XPath("//*[@class='sticker']"));

        private IList<IWebElement> CompareStrip => driver.FindElements(By.XPath("//*[@class='compare-strip-inner']"));
        private IWebElement CloseCompareStripButton => driver.FindElement(By.XPath("//*[@class='compare-strip-close icon-close']"));
        private IWebElement CompareCompareStripButton => driver.FindElement(By.XPath("//*[@class='btn theme-btn']"));
        private IList<IWebElement> RemoveFromCompareStripButton => driver.FindElements(By.XPath("//*[@class='icon-close']"));
        private IList<IWebElement> CompareStripProductBrand => driver.FindElements(By.XPath("//*[@class='compare-item-brand']"));
        private IList<IWebElement> CompareStripProductName => driver.FindElements(By.XPath("//*[@class='compare-item-name']"));

        private IWebElement ComparePopup => driver.FindElement(By.XPath("//*[@class='compare-modal-wrapper']"));
        private IWebElement CloseComparePopupButton => driver.FindElement(By.XPath("//*[@class='compare-modal-header-close icon-close']"));
        private IWebElement AddToComparePopupButton => driver.FindElement(By.XPath("//*[@class='compare-modal-device-add-icon']"));

        private string ProductCardBrandString = ".//h5[@class='brand-name']";
        private string ProductCardNameString = ".//h4[@class='device-name js-device-name']";
        private string ProductCardStickerString = ".//div[@class='sticker']";
        private string ProductCardColorDefaultString = ".//li[@class='color-item default-color']";
        private string ProductCardTariffString = ".//p[@class='recommended-tariff']";
        private string ProductCardCompareButtonString = ".//span[@class='device-item-add-compare']";

        internal bool isGridActive()
        {
            return ClickWeb(PageContainer);
        }

        internal string GetAcivePillColor()
        {
            return PillsActive[0].GetCssValue("background-color");
        }

        internal int GetAcivePillCount()
        {
            return PillsActive.Count;
        }

        internal int GetPillsCount()
        {
            return Pills.Count;
        }

        internal int GetProductsCardsCount()
        {
            return ProductCards.Count;
        }

        internal int GetProductsCanBeAddedToCompareCount()
        {
            return ProductAddToCompareButton.Count;
        }

        internal bool SetPillActive(int index)
        {
            return ClickWeb(Pills[index]);
        }

        internal string GetPillName(int index)
        {
            return Pills[index].Text;
        }

        internal string areCardsContainAllDetails()
        {
            string result = "";
            foreach (var item in ProductCards)
            {
                result = result + isCardContainsAllDetails(item);
            }
            return result;
        }

        internal string isCardContainsAllDetails(IWebElement we)
        {
            string result = "";
            var brand = we.FindElements(By.XPath(ProductCardBrandString));
            result = result + (brand.Count == 1 || brand[0].Text.Length > 0 ? "" : "incorrect brand");

            var name = we.FindElements(By.XPath(ProductCardNameString));
            result = result + (name.Count == 1 || name[0].Text.Length > 0 ? "" : "incorrect name");

            var colorDefault = we.FindElements(By.XPath(ProductCardColorDefaultString));
            result = result + (colorDefault.Count == 1 ? "" : "incorrect default color");

            var compare = we.FindElements(By.XPath(ProductCardCompareButtonString));
            result = result + (compare.Count == 1 || compare[0].Text.Equals(SiteGuiData.ProductCardCompareButton) ? "" : "incorrect compare button");

            var tariff = we.FindElements(By.XPath(ProductCardTariffString));
            result = result + (tariff.Count == 1 || tariff[0].Text.Length > 0 ? "" : "incorrect tariff name");

            return (result.Length > 0 ? string.Format("//ProductCard {0} {1} is incorrect: ", brand[0].Text, name[0].Text) + result : "");
        }

        internal string GetProductCardBrand(int index)
        {
            return ProductCardBrand[index].Text;
        }

        internal string GetProductCardName(int index)
        {
            return ProductCardName[index].Text;
        }

        internal int GetProductCardStickersCount(int index)
        {
            var productCard = ProductCards[index];
            var sticker = productCard.FindElements(By.XPath(ProductCardStickerString));
            return sticker.Count;
        }

        internal int GetProductStickersCount()
        {
            return ProductStickers.Count;
        }

        internal string GetProductStickerName(int index)
        {
            return ProductStickers[index].Text;
        }

        internal bool isCompareStripDisplayed()
        {
            return isWebExists(CompareStrip);
        }

        internal bool isCompareStripNotDisplayed()
        {
            return isWebNotExists(CompareStrip);
        }

        internal bool ClickProductToCompareButton(int index)
        {
            return ClickWeb(ProductAddToCompareButton[index]);
        }

        internal bool ClickProductRemoveFromCompareButton(int index)
        {
            return ClickWeb(ProductIsAddedToCompareButton[index]);
        }

        internal bool RemoveProductFromCompare(int index)
        {
            return ClickWeb(RemoveFromCompareStripButton[index]);
        }

        internal void RemoveProductsFromCompare()
        {
            for (int i = 0; i < RemoveFromCompareStripButton.Count; i++)
            {
                RemoveProductFromCompare(i);
            }
        }

        internal bool CloseCompareStrip()
        {
            return ClickWeb(CloseCompareStripButton);
        }

        internal bool isOpenComparePopupButtonEnabled()
        {
            return isWebEnabled(CompareCompareStripButton);
        }

        internal bool isProductCardCompareButtonToAdd(int index)
        {
            return ProductAddToCompareButton[index].GetCssValue("color").Equals(SiteGuiData.ProductCardCompareButtonColor) &
                isWebEnabled(ProductAddToCompareButton[index]);
        }

        internal bool isProductCardCompareButtonIsAdded(int index)
        {
            return ProductIsAddedToCompareButton[index].GetCssValue("color").Equals(SiteGuiData.ProductCardCompareButtonColorIsAdded) &
                isWebEnabled(ProductAddToCompareButton[index]);
        }

        internal string GetCompareProductBrand(int index)
        {
            return CompareStripProductBrand[index].Text;
        }

        internal string GetCompareProductName(int index)
        {
            return CompareStripProductName[index].Text;
        }

        internal bool OpenComparePopup()
        {
            return ClickWeb(CompareCompareStripButton);
        }

        internal bool isComparePopupOpened()
        {
            return isWebDisplayed(ComparePopup);
        }

        internal bool CloseComparePopup()
        {
            return ClickWeb(CloseComparePopupButton);
        }

        internal bool AddProductInComparePopup()
        {
            return ClickWeb(AddToComparePopupButton);
        }
    }
}
