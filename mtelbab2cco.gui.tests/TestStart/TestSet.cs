﻿using System;
using mtelbab2cco.gui.tests.Data;
using mtelbab2cco.gui.tests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace mtelbab2cco.gui.tests
{
    class TestSet
    {
        public IWebDriver driver;
        public AssertsHelper _assertsAccumulator;
        public TestsHelper testsHelper;

        [OneTimeSetUp]
        public void startBrowserBeforeTestSuit()
        {
            var options = new ChromeOptions();
            options.AddArgument("no-sandbox");
            driver = new ChromeDriver(options);
            testsHelper = new TestsHelper(driver);

            var siteURL = SiteData.BaseURL;

            testsHelper.StartDriver();

            Assert.IsTrue(testsHelper.OpenPage(siteURL), string.Format("Site is not opened, expected {0}, but is {1}", siteURL, driver.Url));
        }

        [OneTimeTearDown]
        public void closeBrowserAfterTestSuit()
        {
            driver.Quit();
        }
    }
}

