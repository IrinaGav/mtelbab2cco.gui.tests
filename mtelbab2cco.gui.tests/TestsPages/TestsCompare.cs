﻿using mtelbab2cco.gui.tests.Helpers;
using mtelbab2cco.gui.tests.Data;
using NUnit.Framework;
using mtelbab2cco.gui.tests.Pages;

namespace mtelbab2cco.gui.tests.Tests
{
    class TestsCompare : TestSet
    {
        PlpPage plpPage;

        [SetUp]
        public void SetUp()
        {
            Assert.IsTrue(testsHelper.OpenPage(SiteData.PlpURL), string.Format("Test is interrupted: PLP page is not opened, expected {0}, but is {1}",
                    SiteData.PlpURL, driver.Url));
            plpPage = new PlpPage(driver);

            Assert.IsTrue(plpPage.isPageLoaded(), "Test is interrupted: PLP is not loaded");
            Assert.IsTrue(plpPage.isGridActive(), "Test is interrupted: Product list is not active");

            _assertsAccumulator = new AssertsHelper();
        }

        [Test]
        public void PlpCompare_CheckStripAfterAdding_ShouldBeShown()
        {
            Assert.IsFalse(plpPage.isCompareStripDisplayed(), "Test is interrupted: compare strip is shown");
            int index = AddRandomProductToCompare();

            _assertsAccumulator.Accumulate(() => Assert.IsTrue(plpPage.isCompareStripDisplayed(), 
                string.Format("Test failed: compare strip is not shown after adding a random product {0}", index)));
        }

        [Test]
        public void PlpCompare_CheckStripAfterRemoving_ShouldBeClosed()
        {
            if (!plpPage.isCompareStripDisplayed())
            {
                AddRandomProductToCompare();
            }
            Assert.IsTrue(plpPage.isCompareStripDisplayed(), "Test is interrupted: compare strip is not shown");

            plpPage.RemoveProductsFromCompare();
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(plpPage.isCompareStripNotDisplayed(), "Test failed: compare strip is shown"));
        }

        [Test]
        public void PlpCompare_CheckStripClosing_ShouldBeHidden()
        {
            if (!plpPage.isCompareStripDisplayed())
            {
                AddRandomProductToCompare();
            }
            Assert.IsTrue(plpPage.isCompareStripDisplayed(), "Test is interrupted: compare strip is not shown");

            plpPage.CloseCompareStrip();
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(plpPage.isCompareStripNotDisplayed(), "Test failed: compare strip is shown"));
        }

        [Test]
        public void PlpCompare_CheckGuiDetailsByClickAddToCompare_ShouldReturnCorrectColourBrandName()
        {
            Assert.IsFalse(plpPage.isCompareStripDisplayed(), "Test is interrupted: compare strip is shown");

            int index = testsHelper.GetRandomIndex(0, plpPage.GetProductsCardsCount());
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(plpPage.isProductCardCompareButtonToAdd(index),
                "Test failed: compare button has incorrect colour or is not enabled"));

            // adding the selected product to Compare
            Assert.IsTrue(plpPage.ClickProductToCompareButton(index), string.Format("Test is interrupted: the To Compare button of product {0} cannot be clicked to add product", index));
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(plpPage.isProductCardCompareButtonIsAdded(0),
                "Test failed: compare button in state Is added has incorrect colour or is not enabled"));

            // check that the selected product is added to Compare
            Assert.IsTrue(plpPage.isCompareStripDisplayed(), "Test is interrupted: compare strip is not shown");
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(plpPage.GetProductCardBrand(index), plpPage.GetCompareProductBrand(0),
                "Test failed: Brand name of added product and product in Compare are different"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(plpPage.GetProductCardName(index), plpPage.GetCompareProductName(0),
                "Test failed: Device name of added product and product in Compare are different"));

            // remove the selected product from Compare
            Assert.IsTrue(plpPage.CloseCompareStrip(), "Test is interrupted: the close button in Compare cannot be clicked");
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(plpPage.isProductCardCompareButtonToAdd(index), "Test failed: compare button is not in initial state"));

            // check Compare is closed
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(plpPage.isCompareStripNotDisplayed(), "Test failed: compare strip is shown"));
        }

        [Test]
        public void PlpCompare_CheckButtonOpenComparePopupWithOneProduct_ShouldBeDisabled()
        {
            Assert.IsFalse(plpPage.isCompareStripDisplayed(), "Test is interrupted: compare strip is shown");
            int index = AddRandomProductToCompare();

            Assert.IsTrue(plpPage.isCompareStripDisplayed(), string.Format("Test is interrupted: compare strip is not shown after adding a random product {0}", index));

            _assertsAccumulator.Accumulate(() => Assert.IsFalse(plpPage.isOpenComparePopupButtonEnabled(), "Test failed: the Compare button is not disabled"));
        }

        [Test]
        public void PlpCompare_CheckButtonOpenComparePopupWithTwoProducts_ShouldBeEnabled()
        {
            Assert.IsFalse(plpPage.isCompareStripDisplayed(), "Test is interrupted: compare strip is shown");
            int index = AddRandomProductToCompare();
            Assert.IsTrue(plpPage.isCompareStripDisplayed(), string.Format("Test is interrupted: compare strip is not shown after adding a random {0} product", index));
            plpPage.ClickProductToCompareButton(0);

            _assertsAccumulator.Accumulate(() => Assert.IsTrue(plpPage.isOpenComparePopupButtonEnabled(), "Test failed: the Compare button is not enabled"));
        }

        [Test]
        public void PlpCompare_CheckComparePopupWithTwoProducts_ShouldBeOpened()
        {
            if (!plpPage.isCompareStripDisplayed())
            {
                AddRandomProductToCompare();
            }
            Assert.IsTrue(plpPage.isCompareStripDisplayed(), "Test is interrupted: compare strip is not shown");
            if (!plpPage.isOpenComparePopupButtonEnabled())
            {
                plpPage.ClickProductToCompareButton(0);
            }
            Assert.IsTrue(plpPage.isOpenComparePopupButtonEnabled(), "Test is interrupted: the Compare button is not enabled");

            _assertsAccumulator.Accumulate(() => Assert.IsTrue(plpPage.OpenComparePopup(), "Test failed: the Compare button to open popup cannot be clicked"));
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(plpPage.isComparePopupOpened(), "Test failed: the Compare popup is not opened"));
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(plpPage.CloseComparePopup(), "Test failed: the Compare popup cannot be closed"));
            _assertsAccumulator.Accumulate(() => Assert.IsFalse(plpPage.isComparePopupOpened(), "Test failed: the Compare popup is opened"));
        }

        [Test]
        public void PlpCompare_CheckAddProductInComparePopup_ShouldClosePopup()
        {
            Assert.IsFalse(plpPage.isCompareStripDisplayed(), "Test is interrupted: compare strip is shown");
            AddRandomProductToCompare();
            Assert.IsTrue(plpPage.isCompareStripDisplayed(), "Test is interrupted: compare strip is not shown");
            plpPage.ClickProductToCompareButton(0);
            Assert.IsTrue(plpPage.isOpenComparePopupButtonEnabled(), "Test is interrupted: the Compare button is not enabled");

            Assert.IsTrue(plpPage.OpenComparePopup(), "Test is interrupted: the Compare button to open popup cannot be clicked");
            Assert.IsTrue(plpPage.isComparePopupOpened(), "Test is interrupted: the Compare popup is not opened");

            _assertsAccumulator.Accumulate(() => Assert.IsTrue(plpPage.AddProductInComparePopup(), "Test failed: the Add product in Compare popup button a=cannot be clicked"));
            _assertsAccumulator.Accumulate(() => Assert.IsFalse(plpPage.isComparePopupOpened(), "Test failed: the Compare popup is opened"));
        }

        private int AddRandomProductToCompare()
        {
            int cnt = plpPage.GetProductsCanBeAddedToCompareCount();
            int index = testsHelper.GetRandomIndex(0, cnt);
            plpPage.ClickProductToCompareButton(index);
            return index;
        }

        [TearDown]
        public void TearDown()
        {
            if (plpPage.isCompareStripDisplayed())
            {
                plpPage.CloseCompareStrip();
            }
        }
    }
}
