﻿using mtelbab2cco.gui.tests.Data;
using mtelbab2cco.gui.tests.Helpers;
using mtelbab2cco.gui.tests.Pages;
using NUnit.Framework;

namespace mtelbab2cco.gui.tests.Tests
{
    [TestFixture]
    class TestsFooter : TestSet
    {
        Footer footer;

        [SetUp]
        public void SetUp()
        {
            footer = new Footer(driver);

            Assert.IsTrue(footer.isPageLoaded(), "Test is interrupted: page is not loaded");

            _assertsAccumulator = new AssertsHelper();
        }

        [Test, TestCaseSource(typeof(FeatureTestCaseData), nameof(FeatureTestCaseData.InvalidEmails))]
        public void Footer_CheckEmailInvalid_ShouldReturnError(string email)
        {
            Assert.IsTrue(footer.TypeEmail(email), "Test is interrupted: is not able to type email value");
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(ErrorMessages.IncorrectEmail, footer.GetErrorMessage(), "Test failed: error message is incorrect"));
        }

        [Test, TestCaseSource(typeof(FeatureTestCaseData), nameof(FeatureTestCaseData.ValidEmails))]
        public void Footer_CheckEmailValid_ShouldBtnBeEnabled(string email)
        {
           Assert.IsTrue(footer.TypeEmail(email), "Test is interrupted: is not able to type email value");
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(footer.isEmailSubmitBtnEnabled(), "Test failed: submit button is disabled"));
        }

        [Test, TestCaseSource(typeof(FeatureTestCaseData), nameof(FeatureTestCaseData.ValidEmails))]
        public void Footer_CheckEmailUnique_ShouldReturnNotification(string email)
        {
            string notification = "";
            for (int i = 0; i < 2; i++)
            {
                footer.isPageLoaded();
                Assert.IsTrue(footer.TypeEmail(email), "Test is interrupted: is not able to type email value");
                Assert.IsTrue(footer.ClickEmailSubmitBtn(), "Test is interrupted: submit button cannot be clicked");
                notification = footer.GetNotification();
                if (notification.Length > 0)
                {
                    break;
                }
            }
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(notification.Length > 0, "Test failed: Notification is empty"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(Notifications.NotUniqueEmail, notification, "Test failed: Notification is incorrect"));
        }

        [Test]
        public void Footer_CheckSocialNetsCount_ShouldReturnFour()
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(SiteData.SocialNets.Length, footer.GetSocialNetsCount(), "Test failed"));
        }

        [Test, TestCaseSource(typeof(FeatureTestCaseData), nameof(FeatureTestCaseData.SocialNets))]
        public void Footer_CheckSocialNet_ShouldExist(string name)
        {
            bool exist = false;
            int socialNetsCnt = footer.GetSocialNetsCount();
            for (int i = 0; i < socialNetsCnt; i++)
            {
                exist = footer.isSocialNetExist(i, name);
                if (exist)
                {
                    break;
                }
            }
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(exist, "Test failed: social net is not in the footer line"));
        }

        [Test, TestCaseSource(typeof(FeatureTestCaseData), nameof(FeatureTestCaseData.SocialNets))]
        public void Footer_CheckSocialNet_ShouldOpenSocialNetPage(string name)
        {
            footer.ClickSocialNet(name);
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(driver.Url.Contains(name), "Test failed: social net is not opened"));
            driver.Navigate().Back();
        }

        [TearDown]
        public void TearDown()
        {
            footer.TypeEmail("");
            footer.WaitNotificationClosed();
        }
    }
}
