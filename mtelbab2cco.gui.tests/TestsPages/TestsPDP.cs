﻿using mtelbab2cco.gui.tests.Data;
using mtelbab2cco.gui.tests.Helpers;
using mtelbab2cco.gui.tests.Pages;
using NUnit.Framework;

namespace mtelbab2cco.gui.tests.Tests
{
    [TestFixture]
    class TestsPDP : TestSet
    {
        PdpPage pdpPage;

        [SetUp]
        public void SetUp()
        {
            Assert.IsTrue(testsHelper.OpenPage(SiteData.PdpURL), string.Format("Test is interrupted: PDP page is not opened, expected {0}, but is {1}",
                    SiteData.PdpURL, driver.Url));
            pdpPage = new PdpPage(driver);

            Assert.IsTrue(pdpPage.isPageLoaded(), "Test is interrupted: PDP is not loaded");

            _assertsAccumulator = new AssertsHelper();
        }

        [Test]
        public void PDP_TODO()
        { }

        [TearDown]
        public void TearDown()
        { }
    }
}
