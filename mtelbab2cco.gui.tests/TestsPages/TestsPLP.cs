﻿using mtelbab2cco.gui.tests.Helpers;
using mtelbab2cco.gui.tests.Data;
using NUnit.Framework;
using mtelbab2cco.gui.tests.Pages;

namespace mtelbab2cco.gui.tests.Tests
{
    [TestFixture]
    class TestsPLP : TestSet
    {
        PlpPage plpPage;

        [SetUp]
        public void SetUp()
        {
            Assert.IsTrue(testsHelper.OpenPage(SiteData.PlpURL), string.Format("Test is interrupted: PLP page is not opened, expected {0}, but is {1}",
                    SiteData.PlpURL, driver.Url));
            plpPage = new PlpPage(driver);

            Assert.IsTrue(plpPage.isPageLoaded(), "Test is interrupted: PLP is not loaded");

           _assertsAccumulator = new AssertsHelper();
        }

        [Test]
        public void PLP_CheckPillActive_ShouldBeOneAndRed()
        {
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(SiteGuiData.ActivePillColor,plpPage.GetAcivePillColor(),"Test failed: color of active pill is incorrect"));
            _assertsAccumulator.Accumulate(() => Assert.AreEqual(1, plpPage.GetAcivePillCount(), "Test failed: count of active pill is incorrect"));
        }

        [Test]
        public void PLP_CheckProductCardsCount_ShouldBeAtLeastOneTODO()
        {

            int cnt = plpPage.GetPillsCount();
            for (int i = 0; i < 3; i++)
            {
                var isPillActive = plpPage.SetPillActive(i);
                _assertsAccumulator.Accumulate(() => Assert.IsTrue(isPillActive, string.Format("Pill {0} failed: cannot be made active", plpPage.GetPillName(i))));
                if (isPillActive & plpPage.isPageLoaded())
                {
                    _assertsAccumulator.Accumulate(() => Assert.IsTrue(plpPage.GetProductsCardsCount() > 0, 
                        string.Format("Pill {0} failed: there are no products", plpPage.GetPillName(i))));
                }
                i++;
            }
        }

        [Test]
        public void PLP_CheckProductCards_ShouldExistAllDetailsTODO()
        {
            string result = plpPage.areCardsContainAllDetails();
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(result.Length == 0, string.Format("Test failed: there is at least one card with error. {0}", result)));
        }

        [Test]
        public void PLP_CheckProductCardStickers_ShouldBeLessFour()
        {
            int count = plpPage.GetProductsCardsCount();
            for (int i = 0; i < count; i++)
            {
                _assertsAccumulator.Accumulate(() => Assert.IsTrue(plpPage.GetProductCardStickersCount(i) < 4, 
                    string.Format("Test failed: product {0} {1} has more than 3 stickers", plpPage.GetProductCardBrand(i), plpPage.GetProductCardName(i))));
            }
        }

        [Test]
        public void PLP_CheckStickers_ShouldBeAtLeastOne()
        {
            int result = 0;
            int count = plpPage.GetProductsCardsCount();
            for (int i = 0; i < count; i++)
            {
                result = result + plpPage.GetProductCardStickersCount(i);
            }
            _assertsAccumulator.Accumulate(() => Assert.IsTrue(result > 0, "Test failed: there is no one sticker on PLP "));
        }

        [Test]
        public void PLP_CheckStickerNames_ShouldBeCorrect()
        {
            int count = plpPage.GetProductStickersCount();
            for (int i = 0; i < count; i++)
            {
                var stickerName = plpPage.GetProductStickerName(i);
                _assertsAccumulator.Accumulate(() => Assert.IsTrue(testsHelper.isValueInArray(stickerName, SiteData.PromoStickers),
                    string.Format("Test failed: sticker '{0}' has wrong name", stickerName)));
            }
        }

        [TearDown]
        public void TearDown()
        { }
    }
}
